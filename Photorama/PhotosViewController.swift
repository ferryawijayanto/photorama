//
//  PhotosViewController.swift
//  Photorama
//
//  Created by Ferry Adi Wijayanto on 24/09/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class PhotosViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var store: PhotoStore!
    let photoDataSource = PhotoDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showPhoto"?:
            if let selectedIndexPath = collectionView.indexPathsForSelectedItems?.first {
                let photo = photoDataSource.photos[selectedIndexPath.row]
                let destinationVC = segue.destination as! PhotoInfoViewController
                destinationVC.photo = photo
                destinationVC.store = store
            }
        default:
            preconditionFailure("Unexpected segue identifier")
        }
    }
}

private extension PhotosViewController {
    func initialize() {
        setupCollectionView()
        updateDataSource()
        fetchInterestingPhoto()
    }
    
    func fetchInterestingPhoto() {
        store.fetchInterestingPhotos { photosResult in
            self.updateDataSource()
            self.collectionView.reloadSections(IndexSet(integer: 0))
        }
    }
    
    func setupCollectionView() {
        collectionView.dataSource = photoDataSource
        collectionView.delegate = self
    }
    
    // Fetch photo from core data
    func updateDataSource() {
        store.fetchAllPhotos { photoResult in
            switch photoResult {
            case let .success(photos):
                self.photoDataSource.photos = photos
            case .failure:
                self.photoDataSource.photos.removeAll()
            }
            self.collectionView.reloadSections(IndexSet(integer: 0))
        }
    }
}

extension PhotosViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let photo = photoDataSource.photos[indexPath.row]
        
        // Downloading image data, which could take some time
        store.fetchImage(for: photo) { imageResult in
            // The index path for the photo might have change between
            // the time the request started and finished, so find the most
            // recent index path
            guard
                let photoIndex = self.photoDataSource.photos.firstIndex(of: photo),
                case let .success(image) = imageResult else { return }
            
            let photoIndexPath = IndexPath(item: photoIndex, section: 0)
            
            // When the request finished, only update the cell if it's still visible
            if let cell = self.collectionView.cellForItem(at: photoIndexPath) as? PhotoCollectionViewCell {
                cell.update(with: image)
            }
        }
    }
}

extension PhotosViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 4
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
