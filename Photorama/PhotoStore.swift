//
//  PhotoStore.swift
//  Photorama
//
//  Created by Ferry Adi Wijayanto on 25/09/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import CoreData

// Create an enum of ImageResult to check if there a valid Image data
// or there is an error during the parsing process
enum ImagesResult {
    case success(UIImage)
    case failure(Error)
}

enum PhotoError: Error{
    case imageCreationError
}

// Create an enum of PhotoResult to check if there a valid JSON data
// or there is an error during the parsing process
enum PhotosResult {
    case success([Photo])
    case failure(Error)
}

enum TagsResult {
    case success([Tag])
    case failure(Error)
}

class PhotoStore {
    
    let imageStore = ImageStore()
    
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Photorama")
        container.loadPersistentStores { description, error in
            if let error = error {
                print("Error setting up Core Data: (\(error))")
            }
        }
        return container
    }()
    
    // Setup URLSession Configuration
    private let sessions: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    
    //MARK:- Make a request from FlickrAPI URL
    func fetchInterestingPhotos(completion: @escaping (PhotosResult) -> ()) {
        let url = FlickrAPI.interestingPhotoURL
        let request = URLRequest(url: url)
        
        let task = sessions.dataTask(with: request) { (data, response, error) in
            self.processPhotosRequest(data: data, error: error) { result in
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
        task.resume()
    }
    
    // Process photo data from the web
    private func processPhotosRequest(data: Data?, error: Error?, completion: @escaping (PhotosResult) -> ()) {
        guard let jsonData = data else {
            completion(.failure(error!))
            return
        }
        
        persistentContainer.performBackgroundTask { context in
            let result = FlickrAPI.photos(fromJson: jsonData, into: context)
            
            do {
                try context.save()
            } catch {
                print("Error saving into Core Data: \(error)")
                completion(.failure(error))
                return
            }
            
            // Fetch photo instance associated with the view context
            // and pass them back via the completion handler
            switch result {
            case let .success(photos):
                // Getting an array of all the objectID associated with photo
                // the photoID instance is a string array within the $0 is a type photo
                let photoID = photos.map({ return $0.objectID })
                
                // Create reference of viewContext
                let viewContext = self.persistentContainer.viewContext
                
                // Mapping the photoID to ask viewContext for the object associated with
                // specific object identifier with the $0 is type string
                let viewContextPhoto = photoID.map({ return viewContext.object(with: $0) }) as! [Photo]
                completion(.success(viewContextPhoto))
            case .failure:
                completion(result)
            }
        }
    }
    
    // Fetch photo that being save in core data than sort item by date
    func fetchAllPhotos(completion: @escaping (PhotosResult) -> ()) {
        let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
        let sortByDateTaken = NSSortDescriptor(key: #keyPath(Photo.dateTaken), ascending: true)
        fetchRequest.sortDescriptors = [sortByDateTaken]
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            do {
                let allPhotos = try viewContext.fetch(fetchRequest)
                completion(.success(allPhotos))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    // MARK:- Download Image Data
    func fetchImage(for photo: Photo, completion: @escaping (ImagesResult) -> ()) {
        guard let photoKey = photo.photoID else {
            preconditionFailure("Photo expected to have a photoID")
        }
        if let image = imageStore.image(forKey: photoKey) {
            OperationQueue.main.addOperation {
                completion(.success(image))
            }
            return
        }
        
        guard let photoURL = photo.remoteURL else {
            preconditionFailure("Photo expected to have a remoteURL")
        }
        let request = URLRequest(url: photoURL as URL)
        
        let task = sessions.dataTask(with: request) { data, response, error in
            let result = self.processImageRequest(data: data, error: error)
            
            if case let .success(image) = result {
                self.imageStore.setImage(image, forKey: photoKey)
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                print(httpResponse.statusCode)
                let headerFields = httpResponse.allHeaderFields as? [String: String]
                print(headerFields!)
            }
            
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
        task.resume()
    }
    
    // Process image data from the web
    private func processImageRequest(data: Data?, error: Error?) -> ImagesResult {
        guard
            let imageData = data,
            let image = UIImage(data: imageData) else {
                // Couldn't create an image
                if data == nil {
                    return .failure(error!)
                } else {
                    return .failure(PhotoError.imageCreationError)
                }
        }
        return .success(image)
    }
    
    // MARK:- Fetch All Tags
    func fetchAllTags(completion: @escaping (TagsResult) -> ()) {
        let fetchRequest: NSFetchRequest<Tag> = Tag.fetchRequest()
        let sortByName = NSSortDescriptor(key: #keyPath(Tag.name), ascending: true)
        
        fetchRequest.sortDescriptors = [sortByName]
        
        let viewContext = persistentContainer.viewContext
        viewContext.perform {
            do {
                let allTags = try fetchRequest.execute()
                completion(.success(allTags))
            } catch {
                completion(.failure(error))
            }
        }
    }
}
